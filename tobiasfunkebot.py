#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import random
import os

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text("Hello")


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text("This bot pulls quotes from Tobias Funke and responds to a small percentage of questions asked in the channel with a quote")


def getquote():
    listOfQuotes = ['Oh, I hope so. Um, I\'m looking for something that says, "Dad likes leather."',
            'Oh, is there such a thing?',
            'The Gothic Castle',
            'Well, I don\'t want to blame it all on 9/11, but it certainly didn\'t help.',
            'I\'m afraid that this offer comes off the table at midnight.',
            'No, it never does. I mean, these people somehow delude themselves into thinking it might, but... but it might work for us.',
            'I\'m afraid I just blue myself.',
            'I know you\'re the big marriage expert. Oh, I\'m sorry, I forgot your wife is dead!',
            'Here he comes. Here comes John Wayne. I\'m not gonna cry about my Pa. I\'m gonna build an airport. Put my name on it.',
            'I\'m afraid I prematurely shot my wad on what was supposed to be a dry run if you will, so I\'m afraid I have something of a mess on my hands.',
            'Well, Michael, you really are quite the Cupid, aren’t you? I tell you, you can sink your arrow into my buttocks any time.',
            'And I apologize for that. I thought it was a pool toy.',
            'Even if it means me taking a chubby, I will suck it up!',
            'I\'ve been in the film business for a while but I just cant seem to get one in the can.',
            'I wouldn\'t mind kissing that man between the cheeks.',
            'Tobias... you blowhard!',
            'You know, mother Lucille, there\'s a psychological concept known as denial that I believe you\'re evincing. It\'s when a thought is so hateful that the mind literally rejects it.',
            'Well, if she\'s not going to say anything, I certainly can\'t help her.',
            'And this time, no tears!',
            'I cannot survive under the house. Perhaps an attic shall I seek.',
            'There\'s the woman I\'m sexually attracted to.',
            'I should call the Hot Cops and tell them to dress up as something more nautically themed. Hot sailors, maybe. Or better yet, hot seam—',
            ]
    randomIndex = random.randint(0, len(listOfQuotes) - 1)
    return listOfQuotes[randomIndex]

def lurk(bot, update):
    if "?" not in update.message.text:
        return 
    if random.random() < 0.2:
        quote = getquote()
        update.message.reply_text(quote)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    apiToken = os.environ.get("API_TOKEN")
    updater = Updater(apiToken)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, lurk))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
