FROM python:2.7.9
COPY requirements.txt /src/
COPY tobiasfunkebot.py /src/
WORKDIR /src
RUN pip install -r requirements.txt
#ENV API_TOKEN "DEFAULT"
ENTRYPOINT ["python", "/src/tobiasfunkebot.py", "$API_TOKEN"]
